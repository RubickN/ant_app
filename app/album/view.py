from flask import Blueprint
from flask import render_template
import os
from ant_app.config import ImagesConfig
album_bp = Blueprint('album', __name__, static_folder='/')
AGAHNIM_URL = "http://127.0.0.1:8088"


@album_bp.route('/app/album')
def index():
    result = []
    prefix = '../static/images'
    dir = 'weix'
    for image in os.listdir(os.path.join(ImagesConfig.BASE_DIR_PATH, dir)):
        result.append(
            '/'.join([prefix, dir, image])
        )
    return render_template('album.html', images=result)
