
from flask import jsonify, request
from flask import Blueprint
from functools import wraps
from flask import make_response
from ant_app.config import AghanimConfig
import requests
import json
search_bp = Blueprint('search_engine', __name__, static_folder='/')
AGAHNIM_URL = AghanimConfig.SERVICE_URL


def allow_cross_domain(fun):
    @wraps(fun)
    def wrapper_fun(*args, **kwargs):
        rst = make_response(fun(*args, **kwargs))
        rst.headers['Access-Control-Allow-Origin'] = '*'
        rst.headers['Access-Control-Allow-Methods'] = 'PUT,GET,POST,DELETE'
        allow_headers = "Referer,Accept,Origin,User-Agent"
        rst.headers['Access-Control-Allow-Headers'] = allow_headers
        return rst
    return wrapper_fun


@search_bp.route('/app/search', methods=['GET'])
@allow_cross_domain
def search():
    key_words = request.args.get('key_words')
    page = request.args.get('page', 1)
    num = request.args.get('num', 10)
    target_date = request.args.get('target_date', 'none')
    search_url = AGAHNIM_URL + "/searcher/search_summary?key_word=%s&page=%s&num=%s&target_date=%s" % (key_words,
                                                                                                       page,
                                                                                                       num,
                                                                                                       target_date)
    res = requests.get(search_url)
    data = json.loads(res.text)
    for index, d in enumerate(data['data']['data']):
        d.append("/app/detail?key_word=%s&resource_id=%s&date=%s" % (
            key_words,
            d[1],
            d[2]
        ))
        d.append(index)
    return jsonify(
        {'total_page': data['data']['total_page'],
         'total': data['data']['total'],
         'data': data['data']['data'],
         'x_value': data['data']['x_value'],
         'y_value': data['data']['y_value']}

    )