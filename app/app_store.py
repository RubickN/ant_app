# from app import flask_app

from search_engine.view import search_bp
from index.view import index_bp
from album.view import album_bp
from upload.view import upload_bp
from flask import Flask
app = Flask(__name__)
app.register_blueprint(search_bp)
app.register_blueprint(index_bp)
app.register_blueprint(album_bp)
app.register_blueprint(upload_bp)


if __name__ == '__main__':
    app.run(debug=True)