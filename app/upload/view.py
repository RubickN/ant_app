from flask import Blueprint, request
from flask import render_template
from werkzeug.utils import secure_filename
from ant_app.config import ImagesConfig
import os
upload_bp = Blueprint('upload', __name__, static_folder='/')
AGAHNIM_URL = "http://127.0.0.1:8088"


@upload_bp.route('/app/upload')
def index():
    if request.method == "POST":
        f = request.files['file']
        upload_path = os.path.join(ImagesConfig.BASE_DIR_PATH, secure_filename(f.filename))
        f.save(upload_path)
    return render_template('upload.html')


@upload_bp.route('/app/upload/add', methods=['POST', 'GET'])
def add():
    if request.method == "POST":
        for f in request.files.getlist('file'):
            upload_path = os.path.join(ImagesConfig.BASE_DIR_PATH, 'weix', secure_filename(f.filename))
            f.save(upload_path)
    return "success"
