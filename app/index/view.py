from flask import Blueprint
from flask import render_template
import json
import requests
import json
from flask import  request
from ant_app.config import AghanimConfig
index_bp = Blueprint('index', __name__, static_folder='/')
AGAHNIM_URL = AghanimConfig.SERVICE_URL


@index_bp.route('/app/index')
def index():
    current_date_url = AGAHNIM_URL + "/searcher/latest_date"
    current_date = requests.get(current_date_url)
    current_resource_num_url = AGAHNIM_URL + "/searcher/resource_num"
    current_resource_num = requests.get(current_resource_num_url)

    return render_template(
        'index.html',
        latest_date=json.loads(current_date.text)['data']['data'],
        all_resource_num=json.loads(current_resource_num.text)['data']['data']
        )


def decorate_into_html(results):
    results = str(results).replace("&lt;", "<")
    results = str(results).replace("&gt;", ">")
    results = str(results).replace("&amp;gt;", "    ")
    return results

CSS_STYLE = "<style style='text/css'>b{color:black;background-color:red}</style>"


@index_bp.route('/app/detail')
def detail():
    search_html_url = AGAHNIM_URL + "/searcher/search_html?key_word=%s&resource_id=%s"
    key_words = request.args.get('key_word')
    resource_id = request.args.get('resource_id')
    search_html_url = search_html_url % (key_words, resource_id)
    search_html = requests.get(search_html_url)
    search_html = json.loads(search_html.text).get(resource_id)
    search_html = decorate_into_html(search_html)
    return CSS_STYLE + "<" + search_html + ">"
