import os
MAIN_DIR = os.path.join(os.path.dirname(__file__))


class BaseConfig(object):
    CELERY_BROKER_URL = "redis://192.168.139.137:6379/2"
    CELERY_RESULT_BACKEND = "redis://192.168.139.137:6379/3"


class ImagesConfig(object):
    BASE_DIR_PATH = os.path.join(MAIN_DIR, 'static', 'images')


class AghanimConfig(object):
    SERVICE_URL = "http://127.0.0.1:9999"